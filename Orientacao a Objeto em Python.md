Orientação a Objeto em Python
===

- What is this baby?
- Class
- Por que Objetos?
- Composição x Herança
- Sobrescrita
- Polimorfismo
- Reflection

---

What is this baby?
===
Programação orientada a objetos, ou OOP, é um paradigma de programação que fornece um meio de estruturar programas para que propriedades e comportamentos sejam agrupados em objetos individuais.

> E no python tudo é um objeto, até mesmo as classes

---

Classes
===

---

O que é mermo?
===
- "Uma classe nada mais é que um modelo ou um protótipo de um objeto."
- "É estrutura avançada que encapsula tipos primitivos e ações."

---

Definição
===
```
# Definição de Classe em Python 2.x
# Usar a notação CamelCase
class Dog(object):
    pass

# Definicação de Classe em Python 3.x
class Dog:
    pass

```

---

Definição
===
```
class Pessoa(Object):

    def __init__(self,name):
        self.name = name
    
    def calcula_idade(self):
        self.idade = 'idade'

        # Retorna a idade da pessoa
        return self.idade
```

---

Composição
===
- Atributos
  - Classe
  - Instância
- Métodos
  - Classe
  - Instância

---

Atributos
===
```
class Pessoa(Object):
    # Atributos de Classe
    TITULO = ('Dr','Mr','Mrs','Ms')
    PESSOAS_CRIADAS = 0

    # Atributos de Instância
    def __init__(self,name, datadenascimento, email, fone, titulo):
        self.name = name
        self.datadenascimento = datadenascimento

        self.email = email
        self.fone = fone
        self.titulo = titulo

        # Se não fosse um atributo de classe eu precisaria
        # de um instância para armazenar isso.
        Pessoa.PESSOAS_CRIADAS += 1

    (...)
```

---

Métodos de Instância
===
```
class Pessoa(Object):

    (...)

    # Método de Instância.
    def calcula_idade(self):
        # Obtem a data atual
        hoje = datetime.date.today()

        # Calcula a diferença entre o ano da idade do objeto
        #e o ano da data atual.
        idade = hoje.year - self.datadenascimento.year

        # Verifica se a data atual é menor que data do aniversario
        # anual da pessoal se for decrementar -1 da idade.
        if hoje < datetime.date(hoje.year, self.datadenascimento.moth,self.datadenascimento.day):
            idade -= 1

        # Retorna a idade da pessoa
        return idade
```

---

Métodos de Classe
===
```
class Pessoa(Object):
    (...)

    #Atributo utilizado por nosso método de classe.
    PESSOAS_POR_TITULO = {"Dr":0,"Mr":0,"Mrs":0,"Ms":0,'ST':0}

    # Métodos de classe, em vez de self usamos cls
    # Métodos de classe são acessados de instâncias ou
    # de objetos de classe
    @classmethod
    def add_pessoa_titulo(cls, titulo):
        if titulo == "Dr":
            cls.PESSOAS_POR_TITULO["Dr"] += 1
        elif titulo == "Mr":
            cls.PESSOAS_POR_TITULO["Mr"] += 1
        elif titulo == "Mrs":
            cls.PESSOAS_POR_TITULO["Mrs"] += 1
        elif titulo == "Ms":
            cls.PESSOAS_POR_TITULO["Ms"] += 1
        else:
            cls.PESSOAS_POR_TITULO["ST"] += 1
    
    (...)

```

---

Métodos de Classe
===
```
    (...)

    #Atributo utilizado por nosso método de classe.
    PESSOAS_POR_TITULO = {"Dr":0,"Mr":0,"Mrs":0,"Ms":0,'ST':0}

    # Métodos de classe, staticmethod não recebem referência
    # do objeto, isso significa que ele não tem acesso ao
    # restante da classe ou objeto.
    @staticmethod
    def add_pessoa_titulo(titulo):
        if titulo == "Dr":
            Pessoa.PESSOAS_POR_TITULO["Dr"] += 1
        elif titulo == "Mr":
            Pessoa.PESSOAS_POR_TITULO["Mr"] += 1
        elif titulo == "Mrs":
            Pessoa.PESSOAS_POR_TITULO["Mrs"] += 1
        elif titulo == "Ms":
            Pessoa.PESSOAS_POR_TITULO["Ms"] += 1
        else:
            Pessoa.PESSOAS_POR_TITULO["ST"] += 1
    
    (...)
```

---

Propriedades
===
```
class Pessoa(Object):

    (...)

    # Método de Instância.
    # Um método também pode representar uma propriedade de um objeto.
    @property
    def idade(self):
        # Obtem a data atual
        hoje = datetime.date.today()

        # Calcula a diferença entre o ano da idade do objeto
        #e o ano da data atual.
        idade = hoje.year - self.datadenascimento.year

        # Verifica se a data atual é menor que data do aniversario
        # anual da pessoal se for decrementar -1 da idade.
        if hoje < datetime.date(hoje.year, self.datadenascimento.moth,self.datadenascimento.day):
            idade -= 1

        # Retorna a idade da pessoa
        return idade

>>> pessoa.idade
```

---

Despindo o Objeto
===
Em python temos um função especial que nos ajuda a ver a estrutura de um objeto.
```
>>> dir(Pessoa)
>>> dir(pessoa)
```

---

Sobrescrita 
===
Em python podemos sobrescrver um método da classe ou objeto para eles comporta-se diferente, ex. __str__
```
    def __str__(self):
        return "%s".format(self.name)
```

---

Por que Objetos?
===
- Familiaridade
- Encapsulamento
- Reaproveitamento
- Abstração

---

Composição x Agregação x Herança
===

---

Composição
===
É uma maneira de agregar objetos, tornando alguns objetos atributos de outros objetos
Ex:
```
(...)
    datadenascimento = datatime.date(1989,02,02)
(...)
```
> Podemos dizer que uma pessoa tem uma data de nascimento.

---

Composição
===
Relacionamentos como esse podem ser um-para-um, um-para-muitos ou muitos-para-muitos e podem ser unidirecionais ou bidirecionais.
> Na composição temos um relacionamento forte entre os objetos, onde um objeto depende do outro para existir.

---

Agregação
===
Tem basicamente as mesmas características da Composição, só que o objeto pode existir sem o outro.

---

Code this ...
===
```
# Classe que representa um estudantes
class Estudante:
    # Iniciação e definição dos atributos
    def __init__(self, nome, numero):
        self.nome = nome
        self.numero = numero
        # Agregação de cursos
        self.classes = []

    # Método de Classe usado para inscrever
    # um estudante em um curso.
    def increverse(self, curso_iniciado):
        self.classes.append(curso_iniciado)
        curso_iniciado.add_estudante(self)
```

---

In Coding ...
===
```
# Classe que representa um departamento
class Estudante:
    # Iniciação e definição dos atributos
    def __init__(self, numero):
        self.numero = numero
        # Agregação de cursos
        self.classes = []

    # Método de Classe usado para inscrever
    # um estudante em um curso.
    def increverse(self, curso_iniciado):
        self.classes.append(curso_iniciado)
        curso_iniciado.add_estudante(self)
```

---

In Coding ...
===
```
# Representa um curso
class Curso:
    def __init__(self, descricao, codigo_curso, credito, departamento):
        self.descricao = descricao
        self.codigo_curso = codigo_curso
        self.credito = credito
        # Tem um departamento em sua composição
        self.departamento = departamento
        self.departamento.add_curso(self)
        # Agregação de cursos iniciados
        self.cursos_iniciados = []

    def iniciar(self, ano):
        self.cursos_iniciados.append(CursoIniciado(self, ano))
        return self.cursos_iniciados[-1]
```

---

In Coding ...
===
```
# Representa um curso iniciado
class CursoIniciado:
    def __init__(self, curso, ano):
        self.curso = curso
        self.ano = ano
        self.estudantes = []

    def add_estudante(self, estudante):
        self.estudantes.append(estudante)
```

---

Herança
===
- "A herança é uma maneira de organizar objetos em uma hierarquia, do mais geral ao mais específico."
- "Um objeto que herda de outro objeto é considerado um subtipo desse objeto."
- "Como vimos no capítulo anterior, todos os objetos do Python herdam object."

---

Let's coding
===
```
class Pai(object):
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        print('Inicializado a classe Pai')

class Filha(Pai):
    def __init__(self, nome, idade, nome_do_pai):
        super().__init__(nome,idade)
        self.nome_do_pai = nome_do_pai
        print('Inicializado a classe filha')

pai = Pai('Thiago', 30)
filha = Filha('Matheus',2,'Thiago')
```

---

Coding de Novo
===
```
# Classe que representa um estudantes
class Estudante(Pessoa):
    # Iniciação e definição dos atributos
    def __init__(self ,name, datadenascimento, email, fone, titulo, numero):
        super().__init__(name, datadenascimento, email, fone, titulo)
        self.numero = numero
        # Agregação de cursos
        self.classes = []

    # Método de Classe usado para inscrever
    # um estudante em um curso.
    def increverse(self, curso_iniciado):
        self.classes.append(curso_iniciado)
        curso_iniciado.add_estudante(self)
```

---

Polimorfismo
===
"Polimorfismo é a capacidade de um objeto poder ser referenciado de várias formas. (cuidado, polimorfismo não quer dizer que o objeto fica se transformando, muito pelo contrário, um objeto nasce de um tipo e morre daquele tipo, o que pode mudar é a maneira como nos referimos a ele)."

---

Polimorfismo
===
```

```

---
