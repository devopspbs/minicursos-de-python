#!/usr/bin/python
# -*- coding: utf-8 -*-
from classe_estudante import *

# Cria um departamento
departamento = Departamento('Computação Aplicada','CA')

# Cria um curso
curso = Curso('Orientação a Objeto em Python', 'OOP', 4, departamento)

# Cria um estudante
estudante = Estudante(1313)

curso_iniciado = curso.iniciar(2019)

curso_iniciado.add_estudante(estudante)


