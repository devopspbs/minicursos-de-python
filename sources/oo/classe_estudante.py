#!/usr/bin/python
# -*- coding: utf-8 -*-

# Classe que representa um estudantes
class Estudante:
    # Iniciação e definição dos atributos
    def __init__(self, numero):
        self.numero = numero
        # Agregação de cursos
        self.classes = []

    # Método de Classe usado para inscrever
    # um estudante em um curso.
    def increverse(self, curso_iniciado):
        self.classes.append(curso_iniciado)
        curso_iniciado.add_estudante(self)

# Classe que representa um departamento
class Departamento:
    # Iniciação e definição dos atributos
    def __init__(self, nome, codigo_departamento):
        self.nome = nome
        self.codigo_departamento = codigo_departamento
        # Agregação de cursos
        self.cursos = {}

    # Registra um curso 
    def add_curso(self, curso):
        self.cursos[curso.codigo_curso] = curso.codigo_curso
        return self.cursos[curso.codigo_curso]

# Representa um curso
class Curso:
    def __init__(self, descricao, codigo_curso, credito, departamento):
        self.descricao = descricao
        self.codigo_curso = codigo_curso
        self.credito = credito
        # Tem um departamento em sua composição
        self.departamento = departamento
        self.departamento.add_curso(self)
        # Agregação de cursos iniciados
        self.cursos_iniciados = []

    def iniciar(self, ano):
        self.cursos_iniciados.append(CursoIniciado(self, ano))
        return self.cursos_iniciados[-1]        

# Representa um curso iniciado
class CursoIniciado:
    def __init__(self, curso, ano):
        self.curso = curso
        self.ano = ano
        self.estudantes = []

    def add_estudante(self, estudante):
        self.estudantes.append(estudante)
