#!/usr/bin/python
# -*- coding: utf-8 -*-

class Pai(object):
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        print('Inicializado a classe Pai')

class Filha(Pai):
    def __init__(self, nome, idade, nome_do_pai):
        super().__init__(nome,idade)
        self.nome_do_pai = nome_do_pai
        print('Inicializado a classe filha')

pai = Pai('Thiago', 30)
filha = Filha('Matheus',2,'Thiago')

