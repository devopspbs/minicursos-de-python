#!/usr/bin/python
# -*- coding: utf-8 -*-
# Nós usaremos isso para objetos do tipo data
import datetime

class Pessoa:

    # Atributos de Classe
    TITULO = ('Dr','Mr','Mrs','Ms')
    PESSOAS_CRIADAS = 0    
    
    #Atributo utilizado por nosso método de classe.
    PESSOAS_POR_TITULO = {"Dr":0,"Mr":0,"Mrs":0,"Ms":0,'ST':0}

    # Métodos de classe, em vez de self usamos cls
    # Métodos de classe são acessados de instâncias ou
    # de objetos de classe
    @classmethod
    def add_pessoa_titulo(cls, titulo):
        if titulo == "Dr":
            cls.PESSOAS_POR_TITULO["Dr"] += 1
        elif titulo == "Mr":
            cls.PESSOAS_POR_TITULO["Mr"] += 1
        elif titulo == "Mrs":
            cls.PESSOAS_POR_TITULO["Mrs"] += 1
        elif titulo == "Ms":
            cls.PESSOAS_POR_TITULO["Ms"] += 1
        else:
            cls.PESSOAS_POR_TITULO["ST"] += 1

    def __init__(self ,name, datadenascimento, email, fone, titulo):
        self.name = name
        self.datadenascimento = datadenascimento

        self.email = email
        self.fone = fone
        self.titulo = titulo

        # Se não fosse um atributos e métodos de classe eu precisaria
        # de um instância para armazenar isso.
        Pessoa.PESSOAS_CRIADAS = Pessoa.PESSOAS_CRIADAS+1
        # Chamada ao método de classe
        Pessoa.add_pessoa_titulo(titulo)

    def __str__(self):
        return "{}".format(self.name)

    # Método de Instância.
    def calcula_idade(self):
        # Obtem a data atual
        hoje = datetime.date.today()

        # Calcula a diferença entre o ano da idade do objeto
        #e o ano da data atual.
        idade = hoje.year - self.datadenascimento.year

        # Verifica se a data atual é menor que data do aniversario
        # anual da pessoal se for decrementar -1 da idade.
        if hoje < datetime.date(hoje.year, self.datadenascimento.moth,self.datadenascimento.day):
            idade -= 1
        
        # Retorna a idade da pessoa
        return idade

