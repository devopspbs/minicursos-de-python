#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Arquivo dos slid's 
Referencias 
'''

a = [1,2,3] # a referenciando a lista [1,2,3]
b = a       # b passa recebe o valor referenciado por a
a.append(4) # isso muda a lista referenciado por a
print(b)    # Surprise!!!

x = 3   # Criado o 3, nome x referência o 3;
y = x   # Cria o nome y, refência o 3;
y = 4   # Cria a referência para 4 e muda o y.
print(x) # O que vai sair aqui?