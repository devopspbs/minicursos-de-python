#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Arquivo dos slid's 
Tipos de dados até
Regras P/ Nomes - Blah Blah Blah!!!
'''

#Isso é um comentário em linha
x = 100     #Isso é um inteiro
print(x)
x = 5/2     #Isso também será um inteiro
print(x)
y = "Hello" #Isso é uma string
print(y)
y = 'Hello' #Isso também
print(y)
y = "Hello's" #Isso também
print(y)
y = """a'b"c""" #Isso também
print(y)
z = 3.45    #Isso é um float
print(z)

if z == 3.45 or y == "Hello":
    x = x + 1       # Operação de adição
    y = y + "World" #Concatenação de String

print(x)
print(y)

'''
Isso é um comentário
em bloco e também é usado
pelo docstring.
'''